from dataclasses import field, fields
from pyexpat import model
from rest_framework import serializers
from .models import Bienes, Usuarios

class UsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuarios
        fields = (
            'id'
            'nombre',
            'usuario',
            'contrasena',
        )

class BienesSeriealizer(serializers.ModelSerializer):
    class Meta:
        model = Bienes
        fields = (
            'id'
            'articulo'
            'descripcion'
        )