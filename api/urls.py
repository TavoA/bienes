from django.urls import path
from api.models import Usuarios
from .views import UsuarioList, BienesList

urlpatterns = [
    path('usuarios/', UsuarioList.as_view(), name='Usuarios_list'),
    path('bienes/', BienesList.as_view(), name='Bienes'),
]