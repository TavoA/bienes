from msilib.schema import ListView
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect,csrf_exempt
from django.views.generic.edit import FormView
from django.contrib.auth import login,logout, authenticate
from rest_framework.authtoken.models import Token
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib.auth.forms import AuthenticationForm
from rest_framework import generics
from .models import Bienes, Usuarios
from .serializers import BienesSeriealizer, UsuarioSerializer
from django.views import generic
import json

class UsuarioList(generics.ListCreateAPIView):
    queryset = Usuarios.objects.all()
    Serializer_class = UsuarioSerializer

class BienesList(generics.ListCreateAPIView):
    queryset = Bienes.objects.all()
    Serializer_class = BienesSeriealizer

class BienesView(generic.ListView):
    template_name = "bienes.html"
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    # Metodo Get para el modelo Bienes
    def get(self, request,id=0, *args, **kwargs):
        if id>0:
            bn = list(Bienes.objects.filter(id = id).values())
            if len(bn)>0:
                bien = bn[0]
                datos = {'message':'Success','Bienes':bien}
            else:
                datos = {'message':'Bienes no encontrados..'}
            return JsonResponse(datos)
        else:
            bs = list(Bienes.objects.values())
            if len(bs)>0:
                datos = {'message':'Success','Bienes':bs}
            else:
                datos = {'message':'Bienes no encontrados..'}
            return JsonResponse(datos)

    # Metodo Post para realizar inserciones
    def post(self, request):
        jd = json.loads(request.body)
        Bienes.objects.create(articulo=jd['articulo'], descripcion=jd['descripcion'], usuario_id_id=jd['usuario_id'])
        datos = {'message':'Success'}
        return JsonResponse(datos)

    # Metodo Put para actualizar un bien en especifico 
    def put(self, request, id):
        jd = json.loads(request.body)
        bn = list(Bienes.objects.filter(id = id).values())
        if len(bn)>0:
            bien = Bienes.objects.get(id = id)
            bien.articulo = jd['articulo']
            bien.descripcion = jd['descripcion']
            bien.usuario_id_id = jd['usuario_id']
            bien.save()
            datos = {'message':'Success'}
        else:
            datos = {'message':'Bienes no encontrados..'}
        return JsonResponse(datos)

    # Metodo Delete para eliminar un Bien en especifico
    def delete(self, request, id):
        bn = list(Bienes.objects.filter(id=id).values())
        if len(bn)>0:
            bien = Bienes.objects.filter(id=id).delete()
            datos = {'message':'Success'}
            # datos = {'message':'Success','Bienes':bien}
        else:
            datos = {'message':'Bienes no encontrados..'}
        return JsonResponse(datos)

class Login(FormView):
    template_name = "login.html"
    form_class = AuthenticationForm
    success_url = reverse_lazy('api:Usuarios_list')

    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        if(request.user.is_authenticated):
            return HttpResponseRedirect(self.get_success_url())
        else:
            return super(Login, self).dispatch(request, *args, *kwargs)
    
    def form_valid(self, form):
        user = authenticate(user = form.cleaned_data['username'], password = form.cleaned_data['password'])
        token,_ = Token.objects.get_or_create(user = user)
        if token:
            login(self.request, form.get_user())
            return super(Login, self).form_valid(form)
# Create your views here.
