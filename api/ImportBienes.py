#
# Archivo para importar datos con la herramienta Pandas
# Importacion de liberia datetime uso de fecha y hora
# sqlachemy para la conexion a base de datos con Postgresql
# Importacion de la libreria haslib para encryptar contraseña
#
from email import header
import psycopg2
import pandas as pd
import datetime
from sqlalchemy import create_engine
import hashlib
import os, binascii
from backports.pbkdf2 import pbkdf2_hmac

# Cadena de conexion a base de datos Postgresql
# Se crea la conexion
conexionstr = "postgresql+psycopg2://postgres:G2846_@localhost:5432/bienes"
engine = create_engine(conexionstr)

# Variable que almacena nombre del "archivo personalizado"
costumerfile = "Customers Bienes.csv"

# Se obtiene el archivo que contiene los datos y se lee
df = pd.read_csv('C:/Users/catif/Downloads/Prueba Técnica - Django Backend (1).csv')

# Variable global para los campos "created_at, update_at"
# Nueva variable global para los campos "created_at, update_at" con formato
# Usuario insertado previamente
date = datetime.datetime.now()
dateNow = datetime.datetime.strftime(date, '%Y-%m-%d %H:%M:%S')
# Conversión de la contraseña default en sha256
password = hashlib.sha256(b"qwerty1").hexdigest()
# Script inserción de un nuevo usuario
query='INSERT INTO public."api_usuarios" (created_at, updated_at, nombre, usuario, contrasena) VALUES(%s,%s,%s,%s,%s)'
my_data=(dateNow,dateNow,"Axel", "developer", password)
    
id= engine.execute(query,my_data)

# Se ejecuta un select para traer el ultimo valor insertado
# se toma el id para el archivo CSV
rs = engine.execute('SELECT * FROM public."api_usuarios" order by created_at DESC limit 1')
idlast = 0
for row in rs:
    idlast = row['id']

#print(userdata)
# Se crean los nuevos campos que no existen en el archivo
df['created_at'] = dateNow
df['updated_at'] = dateNow
df['usuario_id_id'] = idlast

# Se crea un DataFrame
data = pd.DataFrame(df)
 
# Se genera un nuevo CSV con los campos nuevos
data.to_csv(costumerfile)

# Se lee el documento nuevo, excluyendo los titulos del archivo
dataf = pd.read_csv(costumerfile,parse_dates=True, dayfirst=True, index_col=0)

# Se hace la inserción a la tabla Api_bienes excluyendo los titulos del archivo
dataf.to_sql(name="api_bienes", con=engine, if_exists='append', index=False,index_label=None)