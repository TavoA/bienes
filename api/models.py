#
# Modelos ["Base", "Usuarios", "Bienes"]
#
from django.db import models

# Create your models here.
#Modelo Padre de nombre "Base"
class Base(models.Model):
    id = models.AutoField(primary_key=True, auto_created = True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    #Se activa el modo abstract para heredar campos
    class Meta:
        abstract = True

#Modelo Usuarios
# Herendando campos de modelo "Base"
class Usuarios(Base):
    nombre = models.CharField(max_length=100)
    usuario = models.CharField(max_length=100, unique=True)
    contrasena = models.CharField(max_length=258)

    def _str_(self):
        return '{0},{1},{2}'.format(self.nombre,self.usuario,self.contrasena)

# Modelo Bienes
# Herendando campos de modelo "Base"
# ForeignKey de Modelo Usuarios
class Bienes(Base):
    articulo = models.CharField(max_length=255)
    descripcion = models.CharField(max_length=255, null=True)
    usuario_id = models.ForeignKey(Usuarios, null=True, blank=True, on_delete=models.SET_NULL)